(ns plf06.core
  (:gen-class))

(def original [\a \á \b \c \d \e \é \f \g \h \i \í \j \k \l \m \n \ñ \o \ó
               \p \q \r \s \t \u \ú \ü \v \w \x \y \z
               \A \Á \B \C \D \E \É \F \G \H \I \Í \J \K \L \M \N \Ñ \O \Ó
               \P \Q \R \S \T \U \Ú \Ü \V \W \X \Y \Z
               \0 \1 \2 \3 \4 \! \"
               \# \$ \% \& \' \( \) \* \+ \, \- \. \/ \: \; \< \= \> \? \@
               \[ \\ \] \^ \_ \` \{ \| \} \~ \5 \6 \7 \8 \9 \space]) 

(def aplicado [\k \l \m \n \ñ \o \ó \p \q \r \s \t \u \ú \ü \v \w \x \y \z
               \0 \1 \2 \3 \4 \! \" \# \$ \% \& \' \(
               \K \L \M \N \Ñ \O \Ó \P \Q \R \S \T \U \Ú \Ü \V \W \X \Y \Z
               \0 \1 \2 \3 \4 \! \" \# \$ \% \& \' \(
               \) \* \+ \, \- \. \/
               \: \; \< \= \> \? \@ \[ \\ \] \^ \_ \` \{ \| \} \~ \5 \6 \7
               \8 \9 \a \á \b \c \d \e \é \f \g \h \i \í \j \space]) 




(defn cifrar [x]
   (get (zipmap original aplicado) x)) 
(zipmap original aplicado) ;prueba de las columnas

(def v "") ;cadena para almacenar el cifrado

(defn prueba [z] 
  (if (seq z) ; si no está vacía 
    (str (cifrar (first z)) (prueba (rest z))) 
    (str v )))


(prueba "Canción #72")
(prueba "c AN CIÓN # 7 2")
(prueba (apply str "7" "9"))

(defn -main
  [& args]
  (if (empty? args)
    (println "Error. Favor de ingresar como minimo una cadena para cifrar") ;; ¿A qué se debe el error? ¿Alguna forma de indicarle al usuario la razón?
    (println (prueba (apply str args)))))

